using UnityEngine;
using UnityEngine.UIElements;
using Photon.Realtime;

public class MainMenuScript : MonoBehaviour
{
    private UIDocument mainMenu = null;
    // Start is called before the first frame update
    void Start()
    {
        mainMenu = GetComponent<UIDocument>();

        mainMenu.rootVisualElement.Q<Button>("Join").clicked += Join;
        mainMenu.rootVisualElement.Q<Button>("Create").clicked += Create;
        mainMenu.rootVisualElement.Q<Button>("Quit").clicked += Quit;
    }

    private void Quit()
    {
        UnityEngine.Application.Quit();
    }

    private void Create()
    {
        Debug.LogError("Not Defined Method");
    }

    private void Join()
    {
        Debug.LogError("Not Defined Method");
    }
}
